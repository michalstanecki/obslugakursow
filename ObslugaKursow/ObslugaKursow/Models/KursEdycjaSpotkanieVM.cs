﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObslugaKursow.Models
{
    public class KursEdycjaSpotkanieVM
    {
        public Kurs Kurs { get; set; }
        public Kategoria Kategoria { get; set; }
        public EdycjaKursu EdycjaKursu { get; set; }
        public List<Spotkanie> Spotkania { get; set; }

    }
}