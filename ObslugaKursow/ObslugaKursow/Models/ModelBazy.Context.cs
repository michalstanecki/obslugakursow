﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ObslugaKursow.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class KursyDB : DbContext
    {
        public KursyDB()
            : base("name=KursyDB")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Dokument> Dokumenty { get; set; }
        public virtual DbSet<EdycjaKursu> EdycjeKursu { get; set; }
        public virtual DbSet<Kategoria> Kategorie { get; set; }
        public virtual DbSet<Kurs> Kursy { get; set; }
        public virtual DbSet<Spotkanie> Spotkania { get; set; }
        public virtual DbSet<Uczestnictwo> Uczestnictwa { get; set; }
        public virtual DbSet<Uczestnik> Uczestnicy { get; set; }
    }
}
