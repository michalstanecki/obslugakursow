﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ObslugaKursow.Models
{
    public class StudentMain
    {
        [Key]
        public int StuID { get; set; }
        public string StudentName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public virtual ICollection<StudentDetail> StudentDetials { get; set; }
    }
    public class StudentDetail
    {
        [Key]
        public int DetailID { get; set; }
        public int StuID { get; set; }
        public string Course { get; set; }
        public decimal Credit { get; set; }
        public StudentMain StudentMain { get; set; }
    }

}