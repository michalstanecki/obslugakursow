﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ObslugaKursow.Models;

namespace ObslugaKursow.Controllers
{

    public class TestController : Controller
    {
        // private KursyDB db = new KursyDB();
        //   private TestContext db = new Test1Context();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult Uczestnicz(List<HttpPostedFileBase> files, int IdEdycji)
        {
            string UserID = System.Web.HttpContext.Current.User.Identity.GetUserId();
            KursyDB db = new KursyDB();
            Uczestnik uczestnik = db.Uczestnicy.FirstOrDefault(x =>
                x.AspNetUserId.Equals(UserID));
            
            bool uczestniczy = db.Uczestnictwa.Where(x => x.IdUczestnika == uczestnik.IdUczestnika)
                .Where(x => x.idEdycjiKursu == IdEdycji).Any();
            if (!uczestniczy)
            {
                Uczestnictwo noweUczestnictwo = new Uczestnictwo();
                noweUczestnictwo.DataZgloszenia = DateTime.Now;
                noweUczestnictwo.IdUczestnika = uczestnik.IdUczestnika;
                noweUczestnictwo.DataZgloszenia = DateTime.Now;
                noweUczestnictwo.idEdycjiKursu = IdEdycji;
                noweUczestnictwo.Potwierdzone = false;
                db.Uczestnictwa.Add(noweUczestnictwo);
                db.SaveChanges();
                var path = "";
                foreach (var file in files)
                {
                    if (file != null)
                    {
                        if (file.ContentLength > 0)
                        {
                            if (Path.GetExtension(file.FileName).ToLower() == ".jpg" ||
                                Path.GetExtension(file.FileName).ToLower() == ".jpeg" ||
                                Path.GetExtension(file.FileName).ToLower() == ".png")
                            {
                                Dokument nowyDokument = new Dokument();
                                nowyDokument.DataWczytania = DateTime.Now;
                                nowyDokument.idUczestnictwa = noweUczestnictwo.IdUczestnictwa;
                                nowyDokument.NazwaPliku = "";
                                db.Dokumenty.Add(nowyDokument);
                                db.SaveChanges();
                                path = Path.Combine(Server.MapPath("~/Content/Images"),
                                    nowyDokument.IdDokumentu.ToString() + Path.GetExtension(file.FileName).ToLower());
                                file.SaveAs((path));
                                ViewBag.UploadSuccess = true;
                                db.Dokumenty.FirstOrDefault(x => x.IdDokumentu == nowyDokument.IdDokumentu).NazwaPliku =
                                    nowyDokument.IdDokumentu.ToString() + Path.GetExtension(file.FileName).ToLower();
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }

            return View();
        }
        [AllowAnonymous]
        public JsonResult getProductCategories()
        {
            KursyDB db = new KursyDB();
            List<Kategoria> kategorie = new List<Kategoria>();
            foreach (var dbKategoria in db.Kategorie)
            {
                if (dbKategoria.Nazwa != "")
                {
                    kategorie.Add(
                        new Kategoria() { Nazwa = dbKategoria.Nazwa, IdKategorii = dbKategoria.IdKategorii });
                }
            }

            //categories.Add(new Kategoria() { Nazwa = "kat1", IdKategorii = 1 });
            //categories.Add(new Kategoria() { Nazwa = "kat2", IdKategorii = 2 });
            return new JsonResult { Data = kategorie, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [AllowAnonymous]
        public JsonResult GetKursyKategorii(Kategoria item)
        {
            KursyDB db = new KursyDB();
            List<Kurs> kursy = new List<Kurs>();
            foreach (var dbkurs in db.Kursy.Include(nameof(Kategoria)).Where(x => x.idKategorii == item.IdKategorii))
            {
                if (dbkurs.Nazwa != "")
                {
                    kursy.Add(
                        new Kurs() { Nazwa = dbkurs.Nazwa, IdKursu = dbkurs.IdKursu });
                }
            }

            return new JsonResult { Data = kursy, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [AllowAnonymous]
        public JsonResult GetOpisKursu(Kurs item)
        {
            KursyDB db = new KursyDB();
            string opis = "";
            opis = db.Kursy.FirstOrDefault(x => x.IdKursu == item.IdKursu).Opis;
            return new JsonResult { Data = opis };
        }
        [AllowAnonymous]
        public JsonResult getKategoriaKursu(Kurs item)
        {
            KursyDB db = new KursyDB();
            Kategoria kategoria = new Kategoria();
            db.Kursy.Include(nameof(kategoria));
            var kurs = db.Kursy.FirstOrDefault(x => x.IdKursu == item.IdKursu);

            //categories.Add(new Kategoria() { Nazwa = "kat1", IdKategorii = 1 });
            //categories.Add(new Kategoria() { Nazwa = "kat2", IdKategorii = 2 });
            return new JsonResult { Data = new { status = true, kategoria = kurs.Kategoria.Nazwa, IdKategorii = kurs.Kategoria.IdKategorii } };
        }
        [AllowAnonymous]
        public JsonResult getKursy()

        {
            KursyDB db = new KursyDB();
            List<Kurs> kursy = new List<Kurs>();
            foreach (var dbkurs in db.Kursy)
            {
                if (dbkurs.Nazwa != "")
                {
                    kursy.Add(
                        new Kurs() { Nazwa = dbkurs.Nazwa, IdKursu = dbkurs.IdKursu });
                }
            }

            return new JsonResult { Data = kursy, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public JsonResult save(KursEdycjaSpotkaniaVM item)
        {
            Kategoria kategoriaKursu = new Kategoria();
            KursyDB db = new KursyDB();
            Kurs nowyKurs = new Kurs();
            EdycjaKursu nowaEdycjaKursu = new EdycjaKursu();
            List<Spotkanie> noweSpotkania = new List<Spotkanie>();
            kategoriaKursu.IdKategorii = item.IdKategoriiKursu;
            if (kategoriaKursu.IdKategorii == 0)
            {
                if (db.Kategorie.FirstOrDefault(x => x.Nazwa.Equals(item.NazwaKategorii)) != null)
                {
                    return new JsonResult { Data = new { status = false, wiadomosc = "Podana kategoria już istnieje, wybierz ją z listy" } };
                }
                kategoriaKursu = new Kategoria() { Nazwa = item.NazwaKategorii };
                try
                {
                    db.Kategorie.Add(kategoriaKursu);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                    return new JsonResult { Data = new { status = false, wiadomosc = "Nie udało się dodać kategorii, kursu, edycji, a także spotkań." } };
                }

            }
            if (item.IdKursu == 0)
            {
                if (db.Kursy.FirstOrDefault(x => x.Nazwa.Equals(item.NazwaKursu)) != null)
                {
                    return new JsonResult { Data = new { status = false, wiadomosc = "Podana nazwa kursu już istnieje, wybierz ją z listy" } };
                }
                nowyKurs.Nazwa = item.NazwaKursu;
                nowyKurs.Opis = item.Opis;
                nowyKurs.idKategorii = kategoriaKursu.IdKategorii;
                try
                {
                    db.Kursy.Add(nowyKurs);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return new JsonResult { Data = new { status = false, wiadomosc = "Nie udało się dodać kursu, edycji, a także spotkań." } };
                }
                nowaEdycjaKursu.idKursu = nowyKurs.IdKursu;
            }
            else
            {
                nowaEdycjaKursu.idKursu = item.IdKursu;
            }

            nowaEdycjaKursu.NazwaEdycji = item.NazwaEdycji;
            nowaEdycjaKursu.WarunekUczestnictwa = item.Warunek;

            try
            {
                db.EdycjeKursu.Add(nowaEdycjaKursu);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return new JsonResult { Data = new { status = false, wiadomosc = "Nie udało się dodać edycji ani spotkań." } };
            }

            if (item.Lista == null)
            {
                return new JsonResult { Data = new { status = true, wiadomosc = "Dodano nowy kurs oraz edycję. W przyszłości uzupełnij spotkania." } };
            }
            try
            {
                foreach (Spotkanie spotkanie in item.Lista)
                {
                    db.Spotkania.Add(new Spotkanie()
                    {
                        TerminSpotkania = spotkanie.TerminSpotkania,
                        MiejsceSpotkania = spotkanie.MiejsceSpotkania,
                        idEdycji = nowaEdycjaKursu.IdEdycji
                    });

                }
                db.SaveChanges();
                return new JsonResult { Data = new { status = true, wiadomosc = "Pomyślnie zpaisano wszystkie dane." } };
            }
            catch (Exception ex)
            {
                return new JsonResult { Data = new { status = false, wiadomosc = "Nie udało się dodać spotkań." } };
            }

        }
    }

    public class KursEdycjaSpotkaniaVM
    {
        public string NazwaKursu { get; set; }
        public int IdKursu { get; set; }
        public string NazwaKategorii { get; set; }
        public int IdKategoriiKursu { get; set; }
        public string Warunek { get; set; }
        public string Opis { get; set; }
        public string NazwaEdycji { get; set; }
        public IEnumerable<Spotkanie> Lista { get; set; }

    }

}