﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ObslugaKursow.Models;

namespace ObslugaKursow.Controllers
{
    public class SpotkanieController : Controller
    {
        private KursyDB db = new KursyDB();

        // GET: Spotkanie
        public ActionResult Index()
        {
            var spotkania = db.Spotkania.Include(s => s.EdycjaKursu);
            return View(spotkania.ToList());
        }

        // GET: Spotkanie/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spotkanie spotkanie = db.Spotkania.Find(id);
            if (spotkanie == null)
            {
                return HttpNotFound();
            }
            return View(spotkanie);
        }

        // GET: Spotkanie/Create
        public ActionResult Create()
        {
            ViewBag.idEdycji = new SelectList(db.EdycjeKursu, "IdEdycji", "NazwaEdycji");
            return View();
        }

        // POST: Spotkanie/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdSpotkania,MiejsceSpotkania,TerminSpotkania,idEdycji")] Spotkanie spotkanie)
        {
            if (ModelState.IsValid)
            {
                db.Spotkania.Add(spotkanie);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idEdycji = new SelectList(db.EdycjeKursu, "IdEdycji", "NazwaEdycji", spotkanie.idEdycji);
            return View(spotkanie);
        }

        // GET: Spotkanie/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spotkanie spotkanie = db.Spotkania.Find(id);
            if (spotkanie == null)
            {
                return HttpNotFound();
            }
            ViewBag.idEdycji = new SelectList(db.EdycjeKursu, "IdEdycji", "NazwaEdycji", spotkanie.idEdycji);
            return View(spotkanie);
        }

        // POST: Spotkanie/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdSpotkania,MiejsceSpotkania,TerminSpotkania,idEdycji")] Spotkanie spotkanie)
        {
            if (ModelState.IsValid)
            {
                db.Entry(spotkanie).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idEdycji = new SelectList(db.EdycjeKursu, "IdEdycji", "NazwaEdycji", spotkanie.idEdycji);
            return View(spotkanie);
        }

        // GET: Spotkanie/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spotkanie spotkanie = db.Spotkania.Find(id);
            if (spotkanie == null)
            {
                return HttpNotFound();
            }
            return View(spotkanie);
        }

        // POST: Spotkanie/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Spotkanie spotkanie = db.Spotkania.Find(id);
            db.Spotkania.Remove(spotkanie);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
