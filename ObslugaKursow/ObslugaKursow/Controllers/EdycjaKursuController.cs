﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ObslugaKursow.Models;

namespace ObslugaKursow.Controllers
{
    [Authorize]
    public class EdycjaKursuController : Controller
    {
        private KursyDB db = new KursyDB();

        // GET: EdycjaKursu
        //[Authorize]
        public ActionResult Index()
        {
            var edycjeKursu = db.EdycjeKursu.Include(e => e.Kurs).Include(e=>e.Spotkania);
            return View(edycjeKursu.ToList());
        }

        // GET: EdycjaKursu/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EdycjaKursu edycjaKursu = db.EdycjeKursu.Find(id);
            if (edycjaKursu == null)
            {
                return HttpNotFound();
            }
            return View(edycjaKursu);
        }

        // GET: EdycjaKursu/Create
        public ActionResult Create()
        {
            ViewBag.idKursu = new SelectList(db.Kursy, "IdKursu", "Nazwa");
            return View();
        }

        // POST: EdycjaKursu/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdEdycji,NazwaEdycji,WarunekUczestnictwa,idKursu")] EdycjaKursu edycjaKursu)
        {
            if (ModelState.IsValid)
            {
                db.EdycjeKursu.Add(edycjaKursu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idKursu = new SelectList(db.Kursy, "IdKursu", "Nazwa", edycjaKursu.idKursu);
            return View(edycjaKursu);
        }

        // GET: EdycjaKursu/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EdycjaKursu edycjaKursu = db.EdycjeKursu.Find(id);
            if (edycjaKursu == null)
            {
                return HttpNotFound();
            }
            ViewBag.idKursu = new SelectList(db.Kursy, "IdKursu", "Nazwa", edycjaKursu.idKursu);
            return View(edycjaKursu);
        }

        // POST: EdycjaKursu/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdEdycji,NazwaEdycji,WarunekUczestnictwa,idKursu")] EdycjaKursu edycjaKursu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(edycjaKursu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idKursu = new SelectList(db.Kursy, "IdKursu", "Nazwa", edycjaKursu.idKursu);
            return View(edycjaKursu);
        }

        // GET: EdycjaKursu/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EdycjaKursu edycjaKursu = db.EdycjeKursu.Find(id);
            if (edycjaKursu == null)
            {
                return HttpNotFound();
            }
            return View(edycjaKursu);
        }

        // POST: EdycjaKursu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EdycjaKursu edycjaKursu = db.EdycjeKursu.Find(id);
            db.EdycjeKursu.Remove(edycjaKursu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Contribute(int idEdycji)
        {
            EdycjaKursu edycja = db.EdycjeKursu.FirstOrDefault(x => x.IdEdycji == idEdycji);
            edycja.Kurs = db.Kursy.FirstOrDefault(x => x.IdKursu == edycja.idKursu);
            return View(edycja);
        }
        [HttpPost]
        public ActionResult NowaKategoria(string Nazwa)
        {
            if (Nazwa != "" || !db.Kursy.Any(x => x.Nazwa.Equals(Nazwa)))
            {
                db.Kategorie.Add(new Kategoria { Nazwa = Nazwa });
                db.SaveChanges();
                var x = db.Kategorie.ToList();
            }

            return View(Nazwa);
        }
    }
}
