﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ObslugaKursow.Models;

namespace ObslugaKursow.Controllers
{
    public class KursController : Controller
    {
        private KursyDB db = new KursyDB();

        // GET: Kurs
        //[Authorize]
        public ActionResult Index()
        {
            var kursy = db.Kursy.Include(k => k.Kategoria);
            return View(kursy.ToList());
        }

        // GET: Kurs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kurs kurs = db.Kursy.Find(id);
            if (kurs == null)
            {
                return HttpNotFound();
            }
            return View(kurs);
        }

        // GET: Kurs/Create
        public ActionResult Create()
        {
            ViewBag.idKategorii = new SelectList(db.Kategorie, "IdKategorii", "Nazwa");
            return View();
        }

        // POST: Kurs/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdKursu,Nazwa,Opis,idKategorii")] Kurs kurs)
        {
            if (ModelState.IsValid)
            {
                db.Kursy.Add(kurs);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idKategorii = new SelectList(db.Kategorie, "IdKategorii", "Nazwa", kurs.idKategorii);
            return View(kurs);
        }

        // GET: Kurs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kurs kurs = db.Kursy.Find(id);
            if (kurs == null)
            {
                return HttpNotFound();
            }
            ViewBag.idKategorii = new SelectList(db.Kategorie, "IdKategorii", "Nazwa", kurs.idKategorii);
            return View(kurs);
        }

        // POST: Kurs/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdKursu,Nazwa,Opis,idKategorii")] Kurs kurs)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kurs).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idKategorii = new SelectList(db.Kategorie, "IdKategorii", "Nazwa", kurs.idKategorii);
            return View(kurs);
        }

        // GET: Kurs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kurs kurs = db.Kursy.Find(id);
            if (kurs == null)
            {
                return HttpNotFound();
            }
            return View(kurs);
        }

        // POST: Kurs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kurs kurs = db.Kursy.Find(id);
            db.Kursy.Remove(kurs);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
