﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using ObslugaKursow.Models;

namespace ObslugaKursow.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RolesController : Controller
    {
        ApplicationDbContext context;

        public RolesController()
        {
           context = new ApplicationDbContext();
        }
        // GET: Roles
        /// <summary>
        /// Get All Roles
        /// </summary>
        /// <returns>List of all aplication roles.</returns>
        public ActionResult Index()
        {
            
            var roles = context.Roles.ToList();
            return View(roles);
        }

        public ActionResult Create()
        {
            var role = new IdentityRole();
            return View(role);
        }

        [HttpPost]
        public ActionResult Create(IdentityRole Role)
        {
            context.Roles.Add(Role);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}