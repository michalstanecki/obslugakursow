﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ObslugaKursow.Startup))]
namespace ObslugaKursow
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
